from django.http import JsonResponse

from .models import Attendee


def api_list_attendees(request, conference_id):
    response = []
    attendees = Attendee.objects.all()
    for attendee in attendees:
        response.append(
            {
                "name": attendee.name,
                "href": attendee.get_api_url(),
            }

        )

    return JsonResponse({"attendees": response})


def api_show_attendee(request, id):
    attendee = Attendee.objects.get(id=id)

    return JsonResponse({
        "email": attendee.email,
        "name": attendee.name,
        "company_name": attendee.company_name,
        "created": attendee.created
    })

    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
